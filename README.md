# README #

* The application must be universal (iPhone & iPad for iOS -or- phone & tablet for Android).
* Call the Flickr API and pull in a list of photos for the users current location.
* Display the photos in some form of a list (table, collection/grid, whatever).
* Allow photos in the list to be tapped which will display the photo full screen in a new view.

* When on smaller phone sized devices display the photo full screen, cropping parts of the photo is expected and perfectly OK. Do not show any text that may have accompanied the photo, only show the photo.

* When on a tablet sized device display the photo with a max size of 500x500 and anchored to the top of the view with any necessary padding needed to align the top of the photo below any nav/tool bars. Below the photo, add the text comment that accompanied the photo from the Flickr API. The text should not truncate, it should always show the full text regardless of the amount of text.

* Incorporate some sort of animation.

* There are no restrictions of 3rd party libraries, use what ever you would normally use to complete these tasks.


### Requirements ###
iOS 9 

### Knows issues ###
Image cache is not implemented.