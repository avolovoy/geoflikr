//
//  Util.swift
//  GeoFlikr
//
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import UIKit

let mainTintColor = UIColor(red: 64/256, green: 0/256, blue: 128/256, alpha: 1)
