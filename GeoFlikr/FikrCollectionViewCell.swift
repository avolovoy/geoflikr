//
//  FikrCollectionViewCell.swift
//  GeoFlikr
//
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class FikrCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var flikrImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        flikrImageView.image = nil
    }
}
