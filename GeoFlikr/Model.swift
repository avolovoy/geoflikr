//
//  Model.swift
//  GeoFlikr
//
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import UIKit

struct FlikrImage {
    var title : String
    var pictId: String
    var farm: Int
    var owner: String
    var secret: String
    var server: String
    
    func imageUrlString(size:String = "m") -> String {
        return "http://farm\(farm).staticflickr.com/\(server)/\(pictId)_\(secret)_\(size).jpg"
    }
    
}