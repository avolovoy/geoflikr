//
//  Extensions.swift
//  GeoFlikr
//
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC

var key = "geoFlikrImageKey"

extension UIImageView {
    
    var imageUrl:String {
        get {
            return objc_getAssociatedObject(self, &key)  as? String ?? ""
        }
        set {
            objc_setAssociatedObject(self, &key, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY)
        }
    }
    
    func downloadImage (url : String) {
        if let downloadUrl =  NSURL(string: url) {
            self.imageUrl = url;
            let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
            configuration.URLCache = NSURLCache.sharedURLCache()
            configuration.requestCachePolicy = NSURLRequestCachePolicy.ReturnCacheDataElseLoad
            let session = NSURLSession.sharedSession()
            let request = NSMutableURLRequest (URL: downloadUrl)
            request.HTTPMethod = "GET"
            if let cachedResponse = NSURLCache.sharedURLCache().cachedResponseForRequest(request) {
                print("Cached Image:" + url)
                dispatch_async(dispatch_get_main_queue()) {
                    let downloadedImage = UIImage (data: cachedResponse.data)
                    UIView.transitionWithView(self, duration: 0.3, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
                        self.image = downloadedImage
                        }, completion: nil)
                    
                }

            } else {
                let imageTask = session.downloadTaskWithURL(downloadUrl, completionHandler: { (url, response, error) -> Void in
                    if let urlResponse = response?.URL,
                        let dataUrl = url {
                            if self.imageUrl == urlResponse.absoluteString,
                                let imageData = NSData(contentsOfURL: dataUrl) {
                                    dispatch_async(dispatch_get_main_queue()) {
                                        let downloadedImage = UIImage (data: imageData)
                                        UIView.transitionWithView(self, duration: 0.3, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: { () -> Void in
                                            self.image = downloadedImage
                                            }, completion: nil)
                                        
                                    }
                            }
                    }
                    
                })
                imageTask.resume()
            }


        }

    }
}
