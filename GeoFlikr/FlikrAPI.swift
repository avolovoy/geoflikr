//
//  FlikrAPI.swift
//  GeoFlikr
//
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import Foundation
//2d54244131c684d33be6fc40d7bf7f27
//
//Secret:
//3ba6f1f53f221ce3

//s	small square 75x75
//q	large square 150x150
//t	thumbnail, 100 on longest side
//m	small, 240 on longest side
//n	small, 320 on longest side
//-	medium, 500 on longest side
//z	medium 640, 640 on longest side
//c	medium 800, 800 on longest side†
//b	large, 1024 on longest side*
//h	large 1600, 1600 on longest side†
//k	large 2048, 2048 on longest side†
//o	original image, either a jpg, gif or png, depending on source format

let apiKey = "2d54244131c684d33be6fc40d7bf7f27"
let baseSearchUrl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=" + apiKey + "&format=json&nojsoncallback=1"
let testParams = "&lat=39.015697&lon=-94.565559"
enum FlikrError : String, ErrorType {
    case NODATA = "No data in response"
    case REQUESTFAILED = "Request failed"
    case JSONERROR = "Not valid JSON response"
    case COREDATAERROR = "Core Data Error"
    
}
func searchFlikrForLocation (lat :String, lon:String , completionHandler: ((success: Bool, [FlikrImage]?, NSError?) -> Void)?) {
    let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
    let url = NSURL(string:baseSearchUrl + "&lat=\(lat)&lon=\(lon)")
    print(url)
    let request = NSMutableURLRequest(URL: url!)
    request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
    request.HTTPMethod = "GET"

    let task = session.dataTaskWithRequest(request) {
        data, response, error in
        do {
            guard let httpResponse = response as? NSHTTPURLResponse where
                200 ... 299 ~= httpResponse.statusCode else {
                    throw FlikrError.REQUESTFAILED
            }
            guard let responseData = data  else {
                throw FlikrError.NODATA
            }
            guard let jsonResponse  = try NSJSONSerialization.JSONObjectWithData(responseData, options: []) as? [String:AnyObject] else {
                throw FlikrError.JSONERROR
            }
            let results = try parseJSONDictionary(json: jsonResponse)
            if let completionBlock = completionHandler {
                dispatch_async(dispatch_get_main_queue()){
                    completionBlock(success: true, results, nil)
                }
            }
        } catch  let error as FlikrError {
            print(error.rawValue)
            if let completionBlock = completionHandler {
                dispatch_async(dispatch_get_main_queue()){
                    completionBlock(success: false, nil, nil)
                }
            }
        } catch {
            print(error)
        }
        
    }
    task.resume()
}


func parseJSONDictionary ( json json: Dictionary<String, AnyObject> ) throws -> [FlikrImage]? {
    //print("\(json)")
    var newImageArray :[FlikrImage] = []
    
    guard let query = json["photos"] as? Dictionary<String, AnyObject>,
        let photos = query["photo"] as? [AnyObject] else {
            throw FlikrError.JSONERROR
    }
    
    for photoDict in photos {
        guard let photo = photoDict as?  Dictionary<String, AnyObject>,
            let title = photo["title"] as? String ,
            let farm = photo["farm"] as? Int,
            let imageId = photo["id"] as? String,
            let owner = photo["owner"] as? String,
            let secret = photo["secret"] as? String,
            let server = photo["server"] as? String else {
                throw FlikrError.JSONERROR
        }
        let flikrImage = FlikrImage( title:title, pictId:imageId, farm:farm, owner:owner, secret:secret, server:server )
        newImageArray.append(flikrImage)
    }
    return newImageArray;
}


