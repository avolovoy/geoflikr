//
//  ViewController.swift
//  GeoFlikr
//
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit
import CoreLocation


class ViewController: UIViewController, CLLocationManagerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ProgressViewDelegate {
    private let reuseIdentifier = "imageCell"

    var locationManager :CLLocationManager!
    var latitude : CLLocationDegrees?
    var longitude : CLLocationDegrees?
    var dataSource : [FlikrImage] = []
    var cellSize : CGSize = CGSizeZero

    @IBOutlet weak var flowLayout: PhotoFlowLayout!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var progressView: ProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register cell classes
        let cellNib = UINib(nibName: "FikrCollectionViewCell", bundle: nil)
        self.collectionView!.registerNib(cellNib, forCellWithReuseIdentifier: reuseIdentifier)
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        locationManager.distanceFilter = 500
        locationManager.requestWhenInUseAuthorization()
   
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refreshData", name: UIApplicationWillEnterForegroundNotification, object: nil)
        progressView.delegate = self
        refreshData()
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func configureProgressView(showView showView: Bool, empty: Bool, message:String) {
        progressView.configureProgressView(showView, parentView: view, message: message, isEmpty:empty)
    }
    
    override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator) {
            let transitionToWide = size.width > size.height
            var columns = 1
            if transitionToWide {
                columns = self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiom.Pad ? 4 : 3
            } else {
                columns = self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiom.Pad ? 3 : 2
            }
            let dimensions = (size.width - CGFloat(5 * columns + 5))/CGFloat(columns)
            cellSize =   CGSizeMake(dimensions, dimensions);
            coordinator.animateAlongsideTransition({
                context in
                let transition = CATransition()
                transition.duration = context.transitionDuration()
                
                transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
                transition.type = kCATransitionFade
                self.collectionView.reloadItemsAtIndexPaths(
                    self.collectionView.indexPathsForVisibleItems())
                
                }, completion: nil)
        
    }
    
    // MARK: UICollectionViewDataSource
    
     func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
     func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
     func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! FikrCollectionViewCell
        let photo = dataSource[indexPath.item]
        let url = photo.imageUrlString("m")
        cell.flikrImageView.downloadImage(url)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let photoItem = dataSource[indexPath.item]
        let detailsController = storyboard!.instantiateViewControllerWithIdentifier("PhotoViewController") as! PhotoViewController
        detailsController.flikrPhoto = photoItem
        self.presentViewController(detailsController, animated: true) { () -> Void in
            
        }
    }
    
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGRectZero.size
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
       
        if cellSize == CGSizeZero {
            let landscape = self.collectionView.bounds.size.width > self.collectionView.bounds.size.height
            var columns = 1
            if landscape {
                columns = self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiom.Pad ? 4 : 3
            } else {
                columns = self.traitCollection.userInterfaceIdiom == UIUserInterfaceIdiom.Pad ? 3 : 2
            }
            let dimensions = (self.collectionView.bounds.size.width - CGFloat(5 * columns + 5))/CGFloat(columns)
            cellSize =   CGSizeMake(dimensions, dimensions);

        }
        return cellSize
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 5
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            latitude = location.coordinate.latitude
            longitude = location.coordinate.longitude
            locationManager.stopUpdatingLocation()
            configureProgressView(showView:true, empty: false, message: "Searching images")
            if let lat = latitude, let lon = longitude {
                searchFlikrForLocation(lat.description, lon: lon.description, completionHandler: { (success, images, error) -> Void in
                    if success, let newImages = images {
                        self.dataSource = newImages
                        self.configureProgressView(showView:false, empty: self.dataSource.count == 0, message: "No Images Found")
                        if self.dataSource.count > 0 {
                            self.collectionView.reloadData()
                        }
                    }
                   
                })
            }
           
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print(error)
        self.configureProgressView(showView:false, empty: true, message: "Oops, couldn't get location")

    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        print(status.rawValue)
    }
    
    func refreshData() {
        if CLLocationManager.locationServicesEnabled() {
            self.configureProgressView(showView:true, empty: self.dataSource.count > 0, message: "Getting location")
            locationManager.requestLocation()
        } else {
            self.configureProgressView(showView:false, empty: true, message: "Location Services are disabled.")
         }
    

    }

    

}

