//
//  PhotoViewController.swift
//  GeoFlikr
//
//  Created by Alex Volovoy on 10/13/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {
    
    var flikrPhoto : FlikrImage!
    
    @IBOutlet weak var photoCaption: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapRecodnizer = UITapGestureRecognizer(target: self, action: "dismiss")
        imageView.addGestureRecognizer(tapRecodnizer)
        if let captionView = photoCaption {
            let textViewTapRecodnizer = UITapGestureRecognizer(target: self, action: "dismiss")
            captionView.addGestureRecognizer(textViewTapRecodnizer)
            captionView.text  = flikrPhoto.title
            // if selectable set to false in storyboard font is reset to default.
            captionView.selectable = false

        }
        let url = flikrPhoto.imageUrlString("h")
        imageView.downloadImage(url)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    func dismiss() {
        self.dismissViewControllerAnimated(true, completion: nil)

    }
    override func prefersStatusBarHidden() -> Bool {
        return true
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
