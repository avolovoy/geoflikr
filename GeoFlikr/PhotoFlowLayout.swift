//
//  PhotoFlowLayout.swift
//  GeoFlikr
//
//  Created by Alex Volovoy on 10/13/15.
//  Copyright © 2015 Alexey Volovoy. All rights reserved.
//

import UIKit

class PhotoFlowLayout: UICollectionViewFlowLayout {
    
    override func shouldInvalidateLayoutForBoundsChange(newBounds: CGRect) -> Bool {
        return true
    }

}
